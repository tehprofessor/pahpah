{:ok, _} = Application.ensure_all_started(:ex_machina)

alias PahPah.Accounts
alias PahPah.Accounts.User
alias PahPah.Factory
alias PahPah.HomeVisits
alias PahPah.HomeVisits.Visit
alias PahPah.HomeVisits.VisitFulfillment
alias PahPah.Repo

import Ecto.Query

defmodule H do
  alias PahPah.Accounts
  alias PahPah.Factory
  alias PahPah.HomeVisits

  def user(id \\ nil)

  def user(nil) do
    case Accounts.list_users() do
      [user | _] -> user
      [] = _empty -> Factory.insert(:user_with_minutes)
    end
  end

  def user(id), do: Accounts.get_user(id)

  def users(count \\ nil)
  def users(nil), do: users(5)

  def users(count) do
    case Accounts.list_users() do
      [_ | _] = users -> Enum.slice(users, 0, count)
      [] -> Factory.insert_list(count, :user_with_minutes)
    end
  end

  def visit(id \\ nil)

  def visit(nil) do
    case HomeVisits.list_visits() do
      [visit | _] ->
        visit

      [] = _empty ->
        member = Factory.insert(:user_with_minutes)
        Factory.insert(:visit, member_id: member.id)
    end
  end

  def visit(id), do: HomeVisits.get_visit!(id)

  def visit_fulfillment(id \\ nil)

  def visit_fulfillment(nil) do
    case HomeVisits.list_visit_fulfillments() do
      [visit_fulfillments | _] ->
        visit_fulfillments

      [] = _empty ->
        member = Factory.insert(:user_with_minutes)
        pal = Factory.insert(:user)
        new_visit = Factory.insert(:visit, member_id: member.id)

        Factory.insert(:visit_fulfillment,
          member_id: member.id,
          pal_id: pal.id,
          visit_id: new_visit.id
        )
    end
  end

  def visit_fulfillment(id), do: HomeVisits.get_visit!(id)
end
