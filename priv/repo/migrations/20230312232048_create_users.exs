defmodule PahPah.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    # Enable the case-insensitive postgres extension
    execute "CREATE EXTENSION IF NOT EXISTS citext"

    create table(:users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      # Set `email` to `:citext` so queries on `email` are case-insensitive
      add :email, :citext, null: false
      add :first_name, :string, null: false
      add :last_name, :string, null: false
      add :minutes_available, :integer, null: false

      timestamps()
    end

    # Require users to have unique `email`
    create unique_index(:users, [:email])
  end
end
