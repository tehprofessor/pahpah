defmodule PahPah.Repo.Migrations.CreateVisitFulfillments do
  use Ecto.Migration

  def change do
    create table(:visit_fulfillments, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :visit_id, :uuid, null: false
      add :pal_id, :uuid, null: false
      add :member_id, :uuid, null: false
      add :completed_at, :utc_datetime_usec, null: true

      timestamps()
    end

    create unique_index(:visit_fulfillments, [:visit_id])
    create index(:visit_fulfillments, [:member_id])
    create index(:visit_fulfillments, [:pal_id])
    create index(:visit_fulfillments, [:completed_at])
  end
end
