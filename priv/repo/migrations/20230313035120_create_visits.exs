defmodule PahPah.Repo.Migrations.CreateVisits do
  use Ecto.Migration

  def change do
    create table(:visits, primary_key: false) do
      add :id, :binary_id, primary_key: true
      # Using `references` here (foreign key constraints) would increase the data integrity,
      # by having the database enforce their existence. I typically avoid using them unless
      # exceptionally paranoid about dependent data being removed.
      add :member_id, :uuid, null: false
      add :visit_requested_date, :date, null: false
      add :duration, :integer, null: false
      add :tasks, {:array, :string}, null: false

      timestamps()
    end

    create index(:visits, [:member_id])
    create index(:visits, ["visit_requested_date DESC"])
  end
end
