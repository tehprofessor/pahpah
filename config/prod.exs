import Config

config :pah_pah, PahPahWeb.Endpoint, cache_static_manifest: "priv/static/cache_manifest.json"

config :swoosh, api_client: Swoosh.ApiClient.Finch, finch_name: PahPah.Finch

config :logger, level: :info
