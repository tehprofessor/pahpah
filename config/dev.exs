import Config

# Local PostgreSQL database configuration
config :pah_pah, PahPah.Repo,
  username: System.get_env("PAH_PAH_DEV_DB_USERNAME", "postgres"),
  password: System.get_env("PAH_PAH_DEV_DB_PASSWORD", "postgres"),
  hostname: System.get_env("PAH_PAH_DEV_DB_HOSTNAME", "localhost"),
  database: System.get_env("PAH_PAH_DEV_DB_DATABASE", "pah_pah_dev"),
  stacktrace: true,
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

phoenix_port = String.to_integer(System.get_env("PORT") || "4000")

config :pah_pah, PahPahWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: phoenix_port],
  check_origin: false,
  code_reloader: true,
  debug_errors: true,
  secret_key_base: "w/GlQVNBgXcGkPt31t7/gU7IjMfssKe8CMsWODa2rFsUa27sSBTOzuULvq6disq9",
  watchers: [
    esbuild: {Esbuild, :install_and_run, [:default, ~w(--sourcemap=inline --watch)]},
    tailwind: {Tailwind, :install_and_run, [:default, ~w(--watch)]}
  ]

config :pah_pah, PahPahWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
      ~r"priv/gettext/.*(po)$",
      ~r"lib/pah_pah_web/(controllers|live|components)/.*(ex|heex)$"
    ]
  ]

config :pah_pah, dev_routes: true

config :logger, :console, format: "[$level] $message\n"

config :phoenix, :stacktrace_depth, 20

config :phoenix, :plug_init_mode, :runtime

config :swoosh, :api_client, false
