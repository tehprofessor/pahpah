import Config

config :pah_pah, PahPah.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "pah_pah_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

config :pah_pah, PahPahWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "2ORnH4Dn/4ihK6YGlfH1V2IDDKRWJFKQAxlRwp7jW/CUUbsXuBoeCuQmHPuA8DI6",
  server: false

config :pah_pah, PahPah.Mailer, adapter: Swoosh.Adapters.Test

config :swoosh, :api_client, false

config :logger, level: :warning

config :phoenix, :plug_init_mode, :runtime
