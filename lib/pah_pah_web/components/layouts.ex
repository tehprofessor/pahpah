defmodule PahPahWeb.Layouts do
  use PahPahWeb, :html

  embed_templates "layouts/*"
end
