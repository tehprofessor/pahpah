defmodule PahPahWeb.VisitController do
  @moduledoc """
  The controller for the `/api/visits` endpoints.
  """

  use PahPahWeb, :controller

  alias PahPah.HomeVisits
  alias PahPah.HomeVisits.Visit

  action_fallback PahPahWeb.FallbackController

  def index(conn, _params) do
    visits = HomeVisits.list_visits()
    render(conn, :index, visits: visits)
  end

  def create(conn, %{"visit" => visit_params}) do
    with {:ok, %Visit{} = visit} <- HomeVisits.create_visit(visit_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/visits/#{visit}")
      |> render(:show, visit: visit)
    end
  end

  def show(conn, %{"id" => id}) do
    visit = HomeVisits.get_visit!(id)
    render(conn, :show, visit: visit)
  end

  def update(conn, %{"id" => id, "visit" => visit_params}) do
    visit = HomeVisits.get_visit!(id)

    with {:ok, %Visit{} = visit} <- HomeVisits.update_visit(visit, visit_params) do
      render(conn, :show, visit: visit)
    end
  end

  def delete(conn, %{"id" => id}) do
    visit = HomeVisits.get_visit!(id)

    with {:ok, %Visit{}} <- HomeVisits.delete_visit(visit) do
      send_resp(conn, :no_content, "")
    end
  end
end
