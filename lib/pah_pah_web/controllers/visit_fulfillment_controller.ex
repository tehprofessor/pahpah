defmodule PahPahWeb.VisitFulfillmentController do
  use PahPahWeb, :controller

  alias PahPah.HomeVisits
  alias PahPah.HomeVisits.VisitFulfillment

  action_fallback PahPahWeb.FallbackController

  def index(conn, _params) do
    visit_fulfillments = HomeVisits.list_visit_fulfillments()
    render(conn, :index, visit_fulfillments: visit_fulfillments)
  end

  def create(conn, %{"visit_fulfillment" => visit_fulfillment_params}) do
    with {:ok, %VisitFulfillment{} = visit_fulfillment} <-
           HomeVisits.create_visit_fulfillment(visit_fulfillment_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/visit_fulfillments/#{visit_fulfillment}")
      |> render(:show, visit_fulfillment: visit_fulfillment)
    end
  end

  def show(conn, %{"id" => id}) do
    visit_fulfillment = HomeVisits.get_visit_fulfillment!(id)
    render(conn, :show, visit_fulfillment: visit_fulfillment)
  end

  def update(conn, %{"id" => id, "visit_fulfillment" => visit_fulfillment_params}) do
    visit_fulfillment = HomeVisits.get_visit_fulfillment!(id)

    with {:ok, %VisitFulfillment{} = visit_fulfillment} <-
           HomeVisits.update_visit_fulfillment(visit_fulfillment, visit_fulfillment_params) do
      render(conn, :show, visit_fulfillment: visit_fulfillment)
    end
  end

  def delete(conn, %{"id" => id}) do
    visit_fulfillment = HomeVisits.get_visit_fulfillment!(id)

    with {:ok, %VisitFulfillment{}} <- HomeVisits.delete_visit_fulfillment(visit_fulfillment) do
      send_resp(conn, :no_content, "")
    end
  end
end
