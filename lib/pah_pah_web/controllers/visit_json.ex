defmodule PahPahWeb.VisitJSON do
  alias PahPah.HomeVisits.Visit

  @doc """
  Renders a list of visits.
  """
  def index(%{visits: visits}) do
    %{data: for(visit <- visits, do: data(visit))}
  end

  @doc """
  Renders a single visit.
  """
  def show(%{visit: visit}) do
    %{data: data(visit)}
  end

  defp data(%Visit{} = visit) do
    %{
      id: visit.id,
      member_id: visit.member_id,
      visit_requested_date: visit.visit_requested_date,
      duration: visit.duration,
      tasks: visit.tasks
    }
  end
end
