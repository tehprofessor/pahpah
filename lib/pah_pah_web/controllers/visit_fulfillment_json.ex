defmodule PahPahWeb.VisitFulfillmentJSON do
  alias PahPah.HomeVisits.VisitFulfillment

  @doc """
  Renders a list of visit_fulfillments.
  """
  def index(%{visit_fulfillments: visit_fulfillments}) do
    %{data: for(visit_fulfillment <- visit_fulfillments, do: data(visit_fulfillment))}
  end

  @doc """
  Renders a single visit_fulfillment.
  """
  def show(%{visit_fulfillment: visit_fulfillment}) do
    %{data: data(visit_fulfillment)}
  end

  defp data(%VisitFulfillment{} = visit_fulfillment) do
    %{
      id: visit_fulfillment.id,
      visit_id: visit_fulfillment.visit_id,
      pal_id: visit_fulfillment.pal_id,
      member_id: visit_fulfillment.member_id,
      completed_at: visit_fulfillment.completed_at
    }
  end
end
