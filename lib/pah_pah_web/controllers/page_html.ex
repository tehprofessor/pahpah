defmodule PahPahWeb.PageHTML do
  use PahPahWeb, :html

  embed_templates "page_html/*"
end
