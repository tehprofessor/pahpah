defmodule PahPahWeb.Router do
  use PahPahWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {PahPahWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", PahPahWeb do
    pipe_through :api

    resources "/users", UserController, except: [:new, :edit]
    # The `visits` and `visit_fulfillments` would have paths nested within
    # `/users` resource if there was any form of authentication.
    resources "/visits", VisitController, except: [:new, :edit]
    resources "/visit_fulfillments", VisitFulfillmentController, except: [:new, :edit]
  end

  scope "/", PahPahWeb do
    pipe_through :browser

    get "/", PageController, :home
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:pah_pah, :dev_routes) do
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: PahPahWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
