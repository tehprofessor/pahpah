defmodule PahPah.HomeVisits.Visit do
  @moduledoc "The Visit schema"
  use Ecto.Schema

  import Ecto.Changeset

  @type t :: %__MODULE__{}

  @attrs [:member_id, :visit_requested_date, :duration, :tasks]
  @required [:member_id, :visit_requested_date, :duration, :tasks]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "visits" do
    field :duration, :integer
    field :member_id, Ecto.UUID
    field :tasks, {:array, :string}
    field :visit_requested_date, :date

    timestamps()
  end

  def changeset(visit \\ %__MODULE__{}, attrs) do
    visit
    |> cast(attrs, @attrs)
    |> validate_required(@required)
    |> validate_number(:duration, greater_than_or_equal_to: 0)
  end
end
