defmodule PahPah.HomeVisits.VisitFulfillment do
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{}

  @attrs [:visit_id, :pal_id, :member_id, :completed_at]
  @required [:visit_id, :pal_id, :member_id]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "visit_fulfillments" do
    field :completed_at, :utc_datetime_usec
    field :member_id, Ecto.UUID
    field :pal_id, Ecto.UUID
    field :visit_id, Ecto.UUID

    timestamps()
  end

  def changeset(visit_fulfillment \\ %__MODULE__{}, attrs) do
    visit_fulfillment
    |> cast(attrs, @attrs)
    |> validate_required(@required)
    |> unique_constraint(:visit_id)
  end
end
