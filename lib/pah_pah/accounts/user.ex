defmodule PahPah.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{}

  @attrs [:email, :first_name, :last_name, :minutes_available]
  @required [:email, :first_name, :last_name, :minutes_available]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :minutes_available, :integer

    timestamps()
  end

  def changeset(user \\ %__MODULE__{}, attrs) do
    user
    |> cast(attrs, @attrs)
    |> validate_required(@required)
    |> unique_constraint(:email)
  end
end
