defmodule PahPah.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      PahPahWeb.Telemetry,
      PahPah.Repo,
      {Phoenix.PubSub, name: PahPah.PubSub},
      {Finch, name: PahPah.Finch},
      PahPahWeb.Endpoint
    ]

    opts = [strategy: :one_for_one, name: PahPah.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @impl true
  def config_change(changed, _new, removed) do
    PahPahWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
