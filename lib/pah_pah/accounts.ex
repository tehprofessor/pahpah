defmodule PahPah.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false

  alias PahPah.Accounts.User
  alias PahPah.Repo

  @doc "Returns the list of users."
  @spec list_users() :: [User.t()]
  def list_users do
    from(u in User, order_by: [desc: :inserted_at]) |> Repo.all()
  end

  @doc """
  Gets a single user.

  Returns `nil` if the User does not exist.
  """
  @spec get_user(id :: Ecto.UUID.t()) :: User.t() | nil
  def get_user(id), do: Repo.get(User, id)

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.
  """
  @spec get_user!(id :: Ecto.UUID.t()) :: User.t() | no_return
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Gets a single user by the given column name and value.

  Returns `nil` if the user does not exist.
  """
  @spec get_user_by(column_name :: atom(), column_value :: term()) :: User.t() | nil
  def get_user_by(name, value), do: Repo.get_by(User, [{name, value}])

  @doc "Creates a user."
  @spec create_user(attrs :: map()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def create_user(attrs \\ %{}) do
    default_minutes_available = config!(:default_minutes_available)

    %User{}
    |> User.changeset(attrs)
    |> Ecto.Changeset.put_change(:minutes_available, default_minutes_available)
    |> Repo.insert()
  end

  @doc "Updates a user."
  @spec update_user(user :: User.t(), attrs :: map()) ::
          {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc "Deletes a user."
  @spec delete_user(user :: User.t()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc "Returns an `%Ecto.Changeset{}` for tracking user changes."
  @spec change_user(User.t()) :: Ecto.Changeset.t()
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  def config!(key), do: Application.get_env(:pah_pah, __MODULE__)[key]
end
