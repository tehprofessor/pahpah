defmodule PahPah.Repo do
  use Ecto.Repo,
    otp_app: :pah_pah,
    adapter: Ecto.Adapters.Postgres
end
