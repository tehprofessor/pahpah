defmodule PahPah.HomeVisits do
  @moduledoc """
  The HomeVisits context.
  """

  import Ecto.Query, warn: false

  alias Ecto.Multi
  alias PahPah.Accounts
  alias PahPah.Accounts.User
  alias PahPah.HomeVisits.{Visit, VisitFulfillment}
  alias PahPah.Repo

  @minimum_minutes 1

  @doc """
  Returns the list of visits.
  """
  @spec list_visits() :: [Visit.t()]
  def list_visits do
    Repo.all(Visit)
  end

  @doc "Gets visit and it's member"
  @spec get_visit_and_member(visit_id :: Ecto.UUID.t()) ::
          %{visit: Visit.t(), member: User.t()} | nil
  def get_visit_and_member(visit_id) do
    from(vis in Visit,
      join: mem in User,
      on: mem.id == vis.member_id,
      where: vis.id == ^visit_id,
      select: %{visit: vis, member: mem}
    )
    |> Repo.one()
  end

  @doc """
  Gets a single visit.

  Raises `Ecto.NoResultsError` if the Visit does not exist.
  """
  @spec get_visit!(id :: Ecto.UUID.t()) :: Visit.t() | no_return
  def get_visit!(id), do: Repo.get!(Visit, id)

  @doc """
  Creates a visit record, if the associated member's `minutes_available`
  balance is greater than *zero*.
  """
  @spec create_visit(attrs :: map()) :: {:ok, Visit.t()} | {:error, Ecto.Changeset.t()}
  def create_visit(attrs \\ %{}) do
    case Visit.changeset(attrs) do
      %{valid?: true} = visit_changeset ->
        Repo.transaction(fn ->
          member_id = Ecto.Changeset.get_change(visit_changeset, :member_id)

          # Lock the user row for update while we perform the insert
          query =
            from(user in Accounts.User, where: user.id == ^member_id, lock: "FOR UPDATE NOWAIT")

          case Repo.one(query) do
            %_{minutes_available: minutes} when minutes > @minimum_minutes ->
              Repo.insert!(visit_changeset)

            %_{minutes_available: _} ->
              visit_changeset
              |> Ecto.Changeset.add_error(
                :member_id,
                "member has insufficient minutes available to create visit"
              )
              |> Repo.rollback()

            _ ->
              visit_changeset
              |> Ecto.Changeset.add_error(:member_id, "no member found with the given id")
              |> Repo.rollback()
          end
        end)

      invalid_changeset ->
        {:error, invalid_changeset}
    end
  end

  @doc """
  Updates a visit.
  """
  @spec update_visit(Visit.t(), attrs :: map()) :: Visit.t()
  def update_visit(%Visit{} = visit, attrs) do
    visit
    |> Visit.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a visit.
  """
  @spec delete_visit(Visit.t()) :: {:ok, Visit.t()} | {:error, Ecto.Changeset.t()}
  def delete_visit(%Visit{} = visit) do
    Repo.delete(visit)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking visit changes.
  """
  @spec change_visit(Visit.t()) :: Ecto.Changeset.t()
  def change_visit(%Visit{} = visit, attrs \\ %{}) do
    Visit.changeset(visit, attrs)
  end

  @doc """
  Returns the list of visit_fulfillments.
  """
  @spec list_visit_fulfillments() :: [VisitFulfillment.t()]
  def list_visit_fulfillments do
    from(vf in VisitFulfillment, order_by: [desc: vf.updated_at])
    |> Repo.all()
  end

  @doc """
  Gets a single visit_fulfillment.

  Raises `Ecto.NoResultsError` if the Visit fulfillment does not exist.
  """
  @spec get_visit_fulfillment!(id :: Ecto.UUID.t()) :: VisitFulfillment.t() | no_return
  def get_visit_fulfillment!(id), do: Repo.get!(VisitFulfillment, id)

  @doc """
  Creates a `visit_fulfillment` record, debiting the visit's duration from the member's `minutes_available`
  balance and crediting the pal's `minutes_available` balance the duration minus a 15% fee.
  """
  @spec create_visit_fulfillment(attrs :: map()) ::
          {:ok, VisitFulfillment.t()} | {:error, Ecto.Changeset.t()}
  def create_visit_fulfillment(attrs \\ %{}) do
    case VisitFulfillment.changeset(attrs) do
      %{valid?: true} = fulfillment_changeset ->
        Repo.transaction(fn ->
          member_id = Ecto.Changeset.get_change(fulfillment_changeset, :member_id)
          pal_id = Ecto.Changeset.get_change(fulfillment_changeset, :pal_id)
          visit_id = Ecto.Changeset.get_change(fulfillment_changeset, :visit_id)
          user_ids = [member_id, pal_id]

          # Lock the member and pal rows while we update the minutes available
          # to prevent consistency issues.
          users_query =
            from(member in Accounts.User,
              where: member.id in ^user_ids,
              lock: "FOR UPDATE NOWAIT"
            )

          # Fetch the member and pal
          {member, pal} =
            case Repo.all(users_query) do
              [%{id: ^member_id} = member, %{id: ^pal_id} = pal] ->
                {member, pal}

              [%{id: ^pal_id} = pal, %{id: ^member_id} = member] ->
                {member, pal}

              [%{id: ^pal_id} = _pal] ->
                fulfillment_changeset
                |> Ecto.Changeset.add_error(
                  :member_id,
                  "no member found with the given id"
                )
                |> Repo.rollback()

              [%{id: ^member_id} = _member] ->
                fulfillment_changeset
                |> Ecto.Changeset.add_error(
                  :pal_id,
                  "no member found with the given id"
                )
                |> Repo.rollback()

              [] ->
                fulfillment_changeset
                |> Ecto.Changeset.add_error(
                  :user_ids,
                  "invalid member and pal user ids"
                )
                |> Repo.rollback()
            end

          with %{} = visit <- Repo.get(Visit, visit_id) do
            # The `fee_percentage_multiplier` is used here to enable
            # runtime configuration of the value, making it easy to
            # change (redeploy).
            net_duration = visit.duration * fee_percentage_multiplier()

            # Debit (subtract) the duration from the member's minutes_available.
            # To keep this, simple, users can end u
            member_debit_changeset =
              Accounts.User.changeset(member, %{
                minutes_available: member.minutes_available - visit.duration
              })

            # Credit (add) the pal's `minutes_available` duration minus the 15%
            # overhead fee.
            pal_credit_changeset =
              Accounts.User.changeset(pal, %{
                minutes_available: round(pal.minutes_available + net_duration)
              })

            # Multi helps here most by labeling the various writes making the
            # error handling easier.
            transaction_writes =
              Multi.new()
              |> Multi.update(:member_debit, member_debit_changeset)
              |> Multi.update(:pal_credit, pal_credit_changeset)
              |> Multi.insert(:visit_fulfillment, fulfillment_changeset)

            case Repo.transaction(transaction_writes) do
              {:ok, %{visit_fulfillment: visit_fulfillment}} ->
                visit_fulfillment

              {:error, :visit_fulfillment, failed_changeset, _changes} ->
                Repo.rollback(failed_changeset)

              {:error, failed_operation, _, _} ->
                fulfillment_changeset
                |> Ecto.Changeset.add_error(
                  failed_operation,
                  "failed to update balance"
                )
                |> Repo.rollback()
            end
          else
            nil ->
              fulfillment_changeset
              |> Ecto.Changeset.add_error(
                :visit_id,
                "no visit found with the given id"
              )
              |> Repo.rollback()
          end
        end)

      invalid_changeset ->
        {:error, invalid_changeset}
    end
  end

  @doc "Updates a visit_fulfillment record"
  @spec update_visit_fulfillment(VisitFulfillment.t(), attrs :: map()) ::
          {:ok, VisitFulfillment.t()} | {:error, Ecto.Changeset.t()}
  def update_visit_fulfillment(%VisitFulfillment{} = visit_fulfillment, attrs) do
    visit_fulfillment
    |> VisitFulfillment.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a visit_fulfillment.
  """
  @spec delete_visit_fulfillment(VisitFulfillment.t()) ::
          {:ok, VisitFulfillment.t()} | {:error, Ecto.Changeset.t()}
  def delete_visit_fulfillment(%VisitFulfillment{} = visit_fulfillment) do
    Repo.delete(visit_fulfillment)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking visit_fulfillment changes.
  """
  @spec change_visit_fulfillment(VisitFulfillment.t(), attrs :: map()) :: Ecto.Changeset.t()
  def change_visit_fulfillment(%VisitFulfillment{} = visit_fulfillment, attrs \\ %{}) do
    VisitFulfillment.changeset(visit_fulfillment, attrs)
  end

  def config!(key), do: Application.fetch_env!(:pah_pah, __MODULE__)[key]

  @doc "Returns the discount factor for crediting minutes, default is: 0.85 (15%)"
  @spec fee_percentage_multiplier() :: float()
  def fee_percentage_multiplier, do: (100 - config!(:fee_percent)) / 100.00
end
