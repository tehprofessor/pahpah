defmodule Mix.Tasks.PahPah.Visits.Create do
  @moduledoc ~S"""
  Create a new Visit from the command line.

  ## Usage

  Basic example, creating a visit for a member by email:

      mix pah_pah.visits.create example@example.com \
        --duration 100 \
        --visit-requested-date 2030-01-01 \
        --tasks="do something" \
        --tasks="heck yeah"

  Or by using their user `id`:

      mix pah_pah.visits.create example@example.com \
        --member-id=608ab941-955b-4782-ab41-6f9c25febc7f \
        --duration 100 \
        --visit-requested-date 2030-01-01 \
        --tasks="do something" \
        --tasks="heck yeah"
  """
  @shortdoc "Create a new Visit"

  use Mix.Task

  alias PahPah.Accounts

  @requirements ["app.start"]

  @command_options [
    email: :string,
    member_id: :string,
    duration: :integer,
    tasks: :keep,
    visit_requested_date: :string
  ]

  @impl Mix.Task
  def run(args) do
    # The logging `:dev` is set to `:debug` by default and is pretty noisy
    # for a mix task, this quiets it down to anything relatively important.
    _ = Logger.configure(level: :warn)

    with {:ok, params} <- parse_args(args),
         {:ok, visit} <- create_visit(params) do
      visit_json = PahPahWeb.VisitJSON.show(%{visit: visit}) |> Jason.encode!(pretty: true)

      _ =
        Mix.shell().info("""
        #{IO.ANSI.green()}Success!#{IO.ANSI.reset()} Created visit for member:

        #{visit_json}
        """)

      {:ok, visit}
    else
      {:error, %Ecto.Changeset{} = invalid_visit_changeset} ->
        errors_json =
          PahPahWeb.ChangesetJSON.error(%{changeset: invalid_visit_changeset})
          |> Jason.encode!(pretty: true)

        Mix.shell().error("""
        Failed to parse arguments for command with reason(s):

        #{errors_json}
        """)

      {:error, reason} ->
        Mix.shell().error("Failed to parse arguments for command with reason: #{inspect(reason)}")
    end
  end

  def parse_args(args) do
    case OptionParser.parse(args, strict: @command_options) do
      {options, [email | _], []} ->
        {:ok,
         %{
           email: email,
           member_id: options[:member_id],
           duration: options[:duration],
           tasks: Keyword.get_values(options, :tasks),
           visit_requested_date: options[:visit_requested_date]
         }}

      {options, [], []} ->
        {:ok,
         %{
           email: options[:email],
           member_id: options[:member_id],
           duration: options[:duration],
           tasks: options[:tasks],
           visit_requested_date: options[:visit_requested_date]
         }}

      _ ->
        {:error, :invalid_args}
    end
  end

  defp create_visit(
         %{
           email: _email,
           member_id: member_id,
           duration: duration,
           tasks: [_ | _],
           visit_requested_date: visit_requested_date
         } = params
       )
       when duration > 0 and is_binary(visit_requested_date) do
    case fetch_user(params) do
      nil ->
        {:error, "invalid user id, no user found with id=#{member_id}"}

      %_{} = member ->
        PahPah.HomeVisits.create_visit(%{params | member_id: member.id})
    end
  end

  defp create_visit(_params) do
    {:error, :invalid_or_nil_options}
  end

  defp fetch_user(%{email: email}) when is_binary(email), do: Accounts.get_user_by(:email, email)

  defp fetch_user(%{member_id: member_id}) when is_binary(member_id),
    do: Accounts.get_user(member_id)

  defp fetch_user(_), do: nil
end
