defmodule Mix.Tasks.PahPah.Users.Get do
  @moduledoc ~S"""
  Fetch a user by `email` or `id` from the command line.

  ## Usage

  By `email`:

      mix pah_pah.users.get a@example.com

  By `id`:

      mix pah_pah.users.get --id 88f594f6-d498-4081-9d56-fd13f4dccb57
  """
  @shortdoc "Display a user by id or email"

  use Mix.Task

  alias PahPah.Accounts

  @requirements ["app.start"]

  @command_options [
    email: :string,
    id: :string,
    minutes_available: :integer
  ]

  @impl Mix.Task
  def run(args) do
    # The logging `:dev` is set to `:debug` by default and is pretty noisy
    # for a mix task, this quiets it down to anything relatively important.
    _ = Logger.configure(level: :warn)

    with {:ok, params} <- parse_args(args),
         {:ok, %_{} = user} <- find_user(params) do
      user_json = PahPahWeb.UserJSON.show(%{user: user}) |> Jason.encode!(pretty: true)

      _ =
        Mix.shell().info("""
        #{IO.ANSI.green()}Success!#{IO.ANSI.reset()} Found user:

        #{user_json}
        """)

      {:ok, user}
    else
      {:error, reason} ->
        Mix.shell().error("Failed to parse arguments for command with reason: #{inspect(reason)}")
    end
  end

  def parse_args(args) do
    case OptionParser.parse(args, strict: @command_options) do
      {_options, [email | _], []} ->
        {:ok, %{email: email, id: nil}}

      {options, [], []} ->
        {:ok, %{id: options[:id], email: options[:email]}}

      _ ->
        {:error, :invalid_args}
    end
  end

  defp find_user(params) do
    cond do
      is_binary(params.email) -> {:ok, Accounts.get_user_by(:email, params.email)}
      is_binary(params.id) -> {:ok, Accounts.get_user_by(:id, params.id)}
      true -> {:error, :invalid_or_nil_options}
    end
  end
end
