defmodule Mix.Tasks.PahPah.Visits.Fulfill do
  @moduledoc ~S"""
  Create a new VisitFulfillment from the command line.

  ## Usage

  Basic example, to fulfill a visit for a member:

      mix pah_pah.visits.fulfill
        --member-id=608ab941-955b-4782-ab41-6f9c25febc7f \
        --pal-id=608ab941-955b-4782-ab41-6f9c25febc7f \
        --visit-id=608ab941-955b-4782-ab41-6f9c25febc7f

  Note: The `completed_at` timetamp will be the current time, unless
  explicitly set.
  """
  @shortdoc "Create a new VisitFulfillment"

  use Mix.Task

  alias PahPah.{Accounts, HomeVisits}

  @requirements ["app.start"]

  @command_options [
    member_id: :string,
    pal_id: :string,
    visit_id: :string,
    completed_at: :string
  ]

  @impl Mix.Task
  def run(args) do
    # The logging `:dev` is set to `:debug` by default and is pretty noisy
    # for a mix task, this quiets it down to anything relatively important.
    _ = Logger.configure(level: :warn)

    with {:ok, params} <- parse_args(args),
         {:ok, visit_fulfillment} <- create_visit_fulfillment(params) do
      visit_fulfillment_json =
        PahPahWeb.VisitFulfillmentJSON.show(%{visit_fulfillment: visit_fulfillment})
        |> Jason.encode!(pretty: true)

      _ =
        Mix.shell().info("""
        #{IO.ANSI.green()}Success!#{IO.ANSI.reset()} Created visit_fulfillment for member:

        #{visit_fulfillment_json}
        """)

      {:ok, visit_fulfillment}
    else
      {:error, %Ecto.Changeset{} = invalid_fulfillment_changeset} ->
        errors_json =
          PahPahWeb.ChangesetJSON.error(%{changeset: invalid_fulfillment_changeset})
          |> Jason.encode!(pretty: true)

        Mix.shell().error("""
        Failed to parse arguments for command with reason(s):

        #{errors_json}
        """)

      {:error, reason} ->
        Mix.shell().error("Failed to parse arguments for command with reason: #{inspect(reason)}")
    end
  end

  def parse_args(args) do
    now = DateTime.utc_now() |> DateTime.to_iso8601()

    case OptionParser.parse(args, strict: @command_options) do
      {options, [_ | _], []} ->
        {:ok,
         %{
           member_id: options[:member_id],
           pal_id: options[:pal_id],
           visit_id: options[:visit_id],
           completed_at: options[:completed_at] || now
         }}

      {options, [], []} ->
        {:ok,
         %{
           member_id: options[:member_id],
           pal_id: options[:pal_id],
           visit_id: options[:visit_id],
           completed_at: options[:completed_at] || now
         }}

      _opts ->
        {:error, :invalid_args}
    end
  end

  defp create_visit_fulfillment(
         %{
           member_id: member_id,
           pal_id: pal_id,
           visit_id: visit_id,
           completed_at: completed_at
         } = params
       )
       when is_binary(member_id) and is_binary(pal_id) and is_binary(visit_id) and
              is_binary(completed_at) do
    with %{visit: _visit, member: _member} <- HomeVisits.get_visit_and_member(visit_id),
         %_{} = _pal <- Accounts.get_user(pal_id) do
      HomeVisits.create_visit_fulfillment(params)
    else
      nil ->
        {:error, "Missing member, pal, and/or visit."}
    end
  end

  defp create_visit_fulfillment(_params) do
    {:error, :invalid_or_nil_options}
  end
end
