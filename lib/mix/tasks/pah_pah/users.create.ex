defmodule Mix.Tasks.PahPah.Users.Create do
  @moduledoc ~S"""
  Create a new user from the command line.

  ## Usage

  Basic usage, setting the default minutes available at runtime:

      export PAH_PAH_DEFAULT_MINUTES_AVAILABLE=200
      mix pah_pah.users.create seve@example.com \
          --first-name seve \
          --last-name salazar

  """
  @shortdoc "Create a new user"

  use Mix.Task

  alias PahPah.Accounts

  @requirements ["app.start"]

  @command_options [
    email: :string,
    first_name: :string,
    last_name: :string,
    minutes_available: :integer
  ]

  @impl Mix.Task
  def run(args) do
    # The logging `:dev` is set to `:debug` by default and is pretty noisy
    # for a mix task, this quiets it down to anything relatively important.
    _ = Logger.configure(level: :warn)

    with {:ok, params} <- parse_args(args),
         {:ok, user} <- find_or_create_user(params) do
      user_json = PahPahWeb.UserJSON.show(%{user: user}) |> Jason.encode!(pretty: true)

      _ =
        Mix.shell().info("""
        #{IO.ANSI.green()}Success!#{IO.ANSI.reset()} Created new user:

        #{user_json}
        """)

      {:ok, user}
    else
      {:error, reason} ->
        Mix.shell().error("Failed to parse arguments for command with reason: #{inspect(reason)}")
    end
  end

  def parse_args(args) do
    case OptionParser.parse(args, strict: @command_options) do
      {options, [email | _], []} ->
        {:ok,
         %{
           email: email,
           first_name: options[:first_name],
           last_name: options[:last_name],
           minutes_available:
             options[:minutes_available] || Accounts.config!(:default_minutes_available)
         }}

      {options, [], []} ->
        {:ok,
         %{
           email: options[:email],
           first_name: options[:first_name],
           last_name: options[:last_name],
           minutes_available:
             options[:minutes_available] || Accounts.config!(:default_minutes_available)
         }}

      _ ->
        {:error, :invalid_args}
    end
  end

  defp find_or_create_user(%{email: email, first_name: first_name, last_name: last_name} = params)
       when is_binary(email) and is_binary(first_name) and is_binary(last_name) do
    case Accounts.get_user_by(:email, email) do
      nil -> Accounts.create_user(params)
      %_{} = user -> {:ok, user}
    end
  end

  defp find_or_create_user(_params) do
    {:error, :invalid_or_nil_options}
  end
end
