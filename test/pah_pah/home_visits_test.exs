defmodule PahPah.HomeVisitsTest do
  @moduledoc false

  use PahPah.DataCase

  alias PahPah.HomeVisits
  alias PahPah.HomeVisits.Visit
  alias PahPah.HomeVisits.VisitFulfillment
  alias PahPah.Repo

  setup do
    user = Factory.insert(:user_with_minutes)
    {:ok, user: user}
  end

  describe "list_visits/0" do
    test "success: returns an empty list when there are no visits" do
      assert HomeVisits.list_visits() == []
    end

    test "success: returns all (the) visits", %{user: user} do
      visit = Factory.insert(:visit, member_id: user.id)
      assert HomeVisits.list_visits() == [visit]
    end
  end

  describe "get_visit!/1" do
    test "success: returns the visit with given id", %{user: user} do
      visit = Factory.insert(:visit, member_id: user.id)
      assert HomeVisits.get_visit!(visit.id) == visit
    end
  end

  describe "create_visit!/1" do
    test "success: returns an ok-tuple containing the visit with valid params", %{user: user} do
      valid_attrs = %{
        duration: 42,
        member_id: user.id,
        tasks: ["option1", "option2"],
        visit_requested_date: ~D[2023-03-12]
      }

      assert {:ok, %Visit{} = visit} = HomeVisits.create_visit(valid_attrs)
      assert visit.duration == 42
      assert visit.member_id == user.id
      assert visit.tasks == ["option1", "option2"]
      assert visit.visit_requested_date == ~D[2023-03-12]
    end

    test "error: returns an error-tuple containing an invalid changeset when required fields are `nil`" do
      invalid_attrs = %{member_id: nil, visit_requested_date: nil, duration: nil, tasks: nil}
      assert {:error, changeset} = HomeVisits.create_visit(invalid_attrs)
      # Validate the errors for the required fields
      assert %{duration: ["can't be blank"]} = errors_on(changeset)
      assert %{member_id: ["can't be blank"]} = errors_on(changeset)
      assert %{tasks: ["can't be blank"]} = errors_on(changeset)
      assert %{visit_requested_date: ["can't be blank"]} = errors_on(changeset)
    end

    test "error: returns an error-tuple containing an invalid changeset with an invalid duration" do
      invalid_attrs = %{duration: -1}
      assert {:error, changeset} = HomeVisits.create_visit(invalid_attrs)
      assert %{duration: ["must be greater than or equal to 0"]} = errors_on(changeset)
    end
  end

  describe "update_visit/2" do
    test "success: returns an ok-tuple with the updated visit from valid params", %{user: user} do
      visit = Factory.insert(:visit, member_id: user.id)
      update_attrs = %{duration: 43, tasks: ["option1"], visit_requested_date: ~D[2023-03-13]}

      assert {:ok, %Visit{} = visit} = HomeVisits.update_visit(visit, update_attrs)
      assert visit.duration == 43
      assert visit.tasks == ["option1"]
      assert visit.visit_requested_date == ~D[2023-03-13]
    end

    test "error: returns an error-tuple with an error changeset from invalid params", %{
      user: user
    } do
      visit = Factory.insert(:visit, member_id: user.id)
      invalid_attrs = %{member_id: nil, visit_requested_date: nil, duration: nil, tasks: nil}
      assert {:error, %Ecto.Changeset{}} = HomeVisits.update_visit(visit, invalid_attrs)
      assert visit == HomeVisits.get_visit!(visit.id)
    end
  end

  describe "delete_visit/2" do
    test "success: returns an ok-tuple with the deleted visit", %{user: user} do
      visit = Factory.insert(:visit, member_id: user.id)
      assert {:ok, %Visit{}} = HomeVisits.delete_visit(visit)
      assert_raise Ecto.NoResultsError, fn -> HomeVisits.get_visit!(visit.id) end
    end
  end

  describe "change_visit/2" do
    test "success: returns a visit changeset" do
      visit = Factory.build(:visit)
      assert %Ecto.Changeset{} = HomeVisits.change_visit(visit, %{})
    end
  end

  describe "list_visit_fulfillments/0" do
    test "success: returns all visit_fulfillments" do
      visit_fulfillment = create_visit_fulfillment()
      assert HomeVisits.list_visit_fulfillments() == [visit_fulfillment]
    end
  end

  describe "get_visit_fulfillment!/1" do
    test "success: returns the visit_fulfillment with given id" do
      visit_fulfillment = create_visit_fulfillment()
      assert HomeVisits.get_visit_fulfillment!(visit_fulfillment.id) == visit_fulfillment
    end
  end

  describe "create_visit_fulfillment/1 " do
    test "success: returns an ok-tuple containing the fulfillment with valid data," <>
           " and both the member and pal have had their `minutes_available` correctly adjusted" do
      member = Factory.insert(:user_with_minutes, minutes_available: 200)
      # The pal should end up with a balance of `85` minutes_available, after
      # the 15% fee.
      pal = Factory.insert(:user, minutes_available: 0)
      visit = Factory.insert(:visit, member_id: member.id, duration: 100)

      expected_completed_at = DateTime.utc_now()

      valid_attrs = %{
        completed_at: DateTime.to_iso8601(expected_completed_at),
        member_id: member.id,
        pal_id: pal.id,
        visit_id: visit.id
      }

      assert {:ok, %VisitFulfillment{} = visit_fulfillment} =
               HomeVisits.create_visit_fulfillment(valid_attrs)

      assert visit_fulfillment.completed_at == expected_completed_at
      assert visit_fulfillment.member_id == member.id
      assert visit_fulfillment.pal_id == pal.id
      assert visit_fulfillment.visit_id == visit.id

      updated_member = Repo.reload!(member)
      updated_pal = Repo.reload!(pal)

      # Check member's balance has been correctly debited
      assert updated_member.minutes_available == 100
      # Sanity check
      refute updated_member.minutes_available == member.minutes_available

      # Check pal's balance has been correctly credited
      assert updated_pal.minutes_available == 85
      # Sanity check
      refute updated_pal.minutes_available == pal.minutes_available
    end

    test "error: returns an error-tuple containing an invalid changeset when required fields are `nil`" do
      assert {:error, %Ecto.Changeset{} = changeset} =
               HomeVisits.create_visit_fulfillment(%{member_id: nil, pal_id: nil, visit_id: nil})

      assert %{member_id: ["can't be blank"]} = errors_on(changeset)
      assert %{pal_id: ["can't be blank"]} = errors_on(changeset)
      assert %{visit_id: ["can't be blank"]} = errors_on(changeset)
    end
  end

  describe "update_visit_fulfillment/2 " do
    test "success: returns an ok-tuple containing the updated fulfillment with valid params" do
      visit_fulfillment = create_visit_fulfillment()

      update_attrs = %{
        completed_at: ~U[2023-03-13 06:35:00.000000Z],
        member_id: "7488a646-e31f-11e4-aace-600308960668",
        pal_id: "7488a646-e31f-11e4-aace-600308960668",
        visit_id: "7488a646-e31f-11e4-aace-600308960668"
      }

      assert {:ok, %VisitFulfillment{} = visit_fulfillment} =
               HomeVisits.update_visit_fulfillment(visit_fulfillment, update_attrs)

      assert visit_fulfillment.completed_at == ~U[2023-03-13 06:35:00.000000Z]
      assert visit_fulfillment.member_id == "7488a646-e31f-11e4-aace-600308960668"
      assert visit_fulfillment.pal_id == "7488a646-e31f-11e4-aace-600308960668"
      assert visit_fulfillment.visit_id == "7488a646-e31f-11e4-aace-600308960668"
    end

    test "error: returns an error-tuple containing an invalid changeset when required fields are `nil`" do
      visit_fulfillment = create_visit_fulfillment()
      invalid_attrs = %{pal_id: nil, member_id: nil}

      assert {:error, %Ecto.Changeset{}} =
               HomeVisits.update_visit_fulfillment(visit_fulfillment, invalid_attrs)

      assert visit_fulfillment == HomeVisits.get_visit_fulfillment!(visit_fulfillment.id)
    end
  end

  describe "delete_visit_fulfillment/1 " do
    test "success: deletes the visit_fulfillment" do
      visit_fulfillment = create_visit_fulfillment()
      assert {:ok, %VisitFulfillment{}} = HomeVisits.delete_visit_fulfillment(visit_fulfillment)

      assert_raise Ecto.NoResultsError, fn ->
        HomeVisits.get_visit_fulfillment!(visit_fulfillment.id)
      end
    end
  end

  describe "change_visit_fulfillment/1" do
    test "success: returns a visit_fulfillment changeset" do
      visit_fulfillment = create_visit_fulfillment()
      assert %Ecto.Changeset{} = HomeVisits.change_visit_fulfillment(visit_fulfillment)
    end
  end

  describe "fee_percentage_multiplier/0" do
    test "success: returns the correct fee multiplier" do
      og_env = Application.get_env(:pah_pah, HomeVisits)
      updated_env = Keyword.put(og_env, :fee_percent, 25.0)
      _ = Application.put_env(:pah_pah, HomeVisits, updated_env)
      on_exit(fn -> Application.put_env(:pah_pah, HomeVisits, og_env) end)

      assert HomeVisits.fee_percentage_multiplier() == 0.75
    end
  end

  def create_visit_fulfillment do
    member = Factory.insert(:user_with_minutes)
    pal = Factory.insert(:user)
    visit = Factory.insert(:visit, member_id: member.id)
    Factory.insert(:visit_fulfillment, member_id: member.id, visit_id: visit.id, pal_id: pal.id)
  end
end
