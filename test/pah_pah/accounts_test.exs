defmodule PahPah.AccountsTest do
  @moduledoc false

  use PahPah.DataCase

  alias PahPah.Accounts
  alias PahPah.Accounts.User

  describe "list_users/0" do
    test "success: returns all users" do
      user = Factory.insert(:user_with_minutes)
      assert Accounts.list_users() == [user]
    end
  end

  describe "get_user!/1" do
    test "success: returns the user with given id" do
      user = Factory.insert(:user)
      assert Accounts.get_user!(user.id) == user
    end
  end

  describe "get_user_by/2" do
    test "success: returns the user with given email" do
      user = Factory.insert(:user)
      assert Accounts.get_user_by(:email, user.email) == user
    end

    test "success: returns nil when no matching user is found" do
      assert Accounts.get_user_by(:email, "pickle@rick.com") == nil
    end
  end

  describe "create_user/1" do
    test "success: returns an ok-tuple with valid data" do
      valid_attrs = Factory.string_params_for(:user)
      assert {:ok, %User{} = user} = Accounts.create_user(valid_attrs)
      assert user.email == valid_attrs["email"]
      assert user.first_name == valid_attrs["first_name"]
      assert user.last_name == valid_attrs["last_name"]
      assert user.minutes_available == valid_attrs["minutes_available"]
    end

    test "success: returns an ok-tuple and sets `minutes_available` to the default" do
      default_minutes_available = 3
      og_env = Application.get_env(:pah_pah, Accounts)

      # Configure the environment with the updated default
      updated_env = Keyword.put(og_env, :default_minutes_available, default_minutes_available)
      _ = Application.put_env(:pah_pah, Accounts, updated_env)

      # Restore the original default setting on exit
      on_exit(fn ->
        _ = Application.put_env(:pah_pah, Accounts, og_env)
      end)

      valid_attrs = Factory.string_params_for(:user)

      assert {:ok, %User{minutes_available: ^default_minutes_available}} =
               Accounts.create_user(valid_attrs)
    end

    test "error: returns an error-tuple for non-unique emails" do
      params = Factory.params_for(:user)
      _user = Factory.insert(:user, params)

      assert {:error, %Ecto.Changeset{errors: [email: {"has already been taken", _}]}} =
               Accounts.create_user(params)
    end

    test "error: returns an error-tuple containing an invalid changeset with invalid params" do
      invalid_attrs = %{email: nil, first_name: nil, last_name: nil, minutes_available: nil}
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(invalid_attrs)
    end
  end

  describe "update_user/2" do
    test "success: with valid data updates the user" do
      user = Factory.insert(:user)

      update_attrs = %{
        email: "some updated email",
        first_name: "some updated first_name",
        last_name: "some updated last_name",
        minutes_available: 43
      }

      assert {:ok, %User{} = user} = Accounts.update_user(user, update_attrs)
      assert user.email == "some updated email"
      assert user.first_name == "some updated first_name"
      assert user.last_name == "some updated last_name"
      assert user.minutes_available == 43
    end

    test "error: returns an error-tuple containing an invalid changeset with invalid params" do
      user = Factory.insert(:user)

      invalid_attrs = %{email: nil, first_name: nil, last_name: nil, minutes_available: nil}
      assert {:error, %Ecto.Changeset{} = changeset} = Accounts.update_user(user, invalid_attrs)
      assert %{email: ["can't be blank"]} = errors_on(changeset)
      assert %{first_name: ["can't be blank"]} = errors_on(changeset)
      assert %{last_name: ["can't be blank"]} = errors_on(changeset)
      assert %{minutes_available: ["can't be blank"]} = errors_on(changeset)
      assert user == Accounts.get_user!(user.id)
    end
  end

  describe "delete_user/2" do
    test "success: returns an ok-tuple and deletes the user" do
      user = Factory.insert(:user)
      assert {:ok, %User{}} = Accounts.delete_user(user)
      # Confirm the user deleted
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end
  end

  describe "change_user/1" do
    test "success: returns a user changeset" do
      user = Factory.build(:user)
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
