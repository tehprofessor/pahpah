defmodule PahPah.Factory do
  @moduledoc false

  use ExMachina.Ecto, repo: PahPah.Repo

  alias PahPah.Accounts.User
  alias PahPah.HomeVisits.{Visit, VisitFulfillment}

  def user_factory do
    %User{
      first_name: Faker.Person.En.first_name(),
      last_name: Faker.Person.En.last_name(),
      email: sequence(:email, &"email-#{&1}-#{Faker.Internet.email()}"),
      minutes_available: 0
    }
  end

  def user_with_minutes_factory do
    %{user_factory() | minutes_available: max(10, :rand.uniform(2_000))}
  end

  def uuid, do: Ecto.UUID.generate()

  def visit_factory do
    today = Date.utc_today() |> Date.to_iso8601()

    %Visit{
      member_id: uuid(),
      duration: :rand.uniform(_max_four_hours_in_minutes = 60 * 4),
      visit_requested_date: today,
      tasks: []
    }
  end

  def visit_fulfillment_factory do
    %VisitFulfillment{visit_id: uuid(), pal_id: uuid(), member_id: uuid(), completed_at: nil}
  end
end
