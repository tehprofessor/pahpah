defmodule PahPahWeb.UserControllerTest do
  use PahPahWeb.ConnCase

  alias PahPah.Accounts.User

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "GET - /users" do
    test "success: returns an empty list when there are no users", %{conn: conn} do
      conn = get(conn, ~p"/api/users")
      assert json_response(conn, 200)["data"] == []
    end

    test "success: returns a list of all the users", %{conn: conn} do
      # The results returned by the API are sorted, descending on the
      # `inserted_at` column
      users = Factory.insert_list(2, :user) |> Enum.sort_by(& &1.inserted_at, :desc)
      conn = get(conn, ~p"/api/users")

      assert results = json_response(conn, 200)["data"]

      for {result, user} <- Enum.zip(results, users), {key, actual_value} <- result do
        expected_value = Map.get(user, String.to_atom(key))
        assert actual_value == expected_value
      end
    end
  end

  describe "POST - /users" do
    test "success: creates and returns a user with valid data", %{conn: conn} do
      params = Factory.string_params_for(:user)
      conn = post(conn, ~p"/api/users", user: params)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, ~p"/api/users/#{id}")
      data = json_response(conn, 200)["data"]

      assert user = PahPah.Repo.get(User, data["id"])

      for {key, expected_value} <- params do
        # Check the data returned matches what was expected
        assert data[key] == expected_value
        # Confirm the data persisted matches what was expected
        assert Map.get(user, String.to_atom(key)) == expected_value
      end
    end

    test "error: renders errors when data is invalid", %{conn: conn} do
      invalid_params = %{}
      conn = post(conn, ~p"/api/users", user: invalid_params)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "PUT /users/:id" do
    setup [:create_user]

    test "success: renders user when data is valid", %{conn: conn, user: %User{id: id} = user} do
      params =
        Factory.string_params_for(:user, %{
          first_name: Faker.Person.first_name(),
          last_name: Faker.Person.last_name()
        })

      conn = put(conn, ~p"/api/users/#{user}", user: params)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      data = json_response(conn, 200)["data"]

      for {key, expected_value} <- params do
        assert data[key] == expected_value
      end
    end

    test "error: renders errors when data is invalid", %{conn: conn, user: user} do
      invalid_params = %{"first_name" => nil}
      conn = put(conn, ~p"/api/users/#{user}", user: invalid_params)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "DELETE /users/:id" do
    setup [:create_user]

    test "success: deletes chosen user", %{conn: conn, user: user} do
      conn = delete(conn, ~p"/api/users/#{user}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/users/#{user}")
      end
    end
  end

  defp create_user(_) do
    user = Factory.insert(:user)
    %{user: user}
  end
end
