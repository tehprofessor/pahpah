defmodule PahPahWeb.VisitControllerTest do
  @moduledoc false

  use PahPahWeb.ConnCase

  alias PahPah.HomeVisits.Visit

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "GET /visits" do
    test "success: returns an empty list when there are no visits", %{conn: conn} do
      conn = get(conn, ~p"/api/visits")
      assert json_response(conn, 200)["data"] == []
    end

    test "success: returns a list of all the visits", %{conn: conn} do
      # The results returned by the API are sorted, descending on the
      # `inserted_at` column
      user = Factory.insert(:user_with_minutes)

      visits =
        Factory.insert_list(2, :visit, member_id: user.id)
        |> Enum.sort_by(& &1.inserted_at, :desc)

      conn = get(conn, ~p"/api/visits")

      assert results = json_response(conn, 200)["data"]

      for {result, user} <- Enum.zip(results, visits), {key, actual_value} <- result do
        expected_value =
          case Map.get(user, String.to_atom(key)) do
            %Date{} = date_value -> Date.to_iso8601(date_value)
            val -> val
          end

        assert actual_value == expected_value
      end
    end
  end

  describe "GET /visits/:id" do
    test "success: returns the visit by id", %{conn: conn} do
      user = Factory.insert(:user_with_minutes)
      visit_params = Factory.params_for(:visit, member_id: user.id)
      visit = Factory.insert(:visit, visit_params)

      conn = get(conn, ~p"/api/visits/#{visit}")
      assert response = json_response(conn, 200)["data"]

      for {key, expected_value} <- visit_params do
        assert response[to_string(key)] == expected_value
      end
    end

    test "error: returns a 404 for an unknown visit id", %{conn: conn} do
      unknown_id = Factory.uuid()

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/visits/#{unknown_id}")
      end
    end
  end

  describe "POST /visits" do
    test "success: renders visit when data is valid", %{conn: conn} do
      user = Factory.insert(:user_with_minutes)

      visit_params =
        Factory.string_params_for(:visit,
          tasks: ["nice-task-1", "nicer-task-2"],
          member_id: user.id
        )

      conn = post(conn, ~p"/api/visits", visit: visit_params)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, ~p"/api/visits/#{id}")

      assert response = json_response(conn, 200)["data"]

      for {key, expected_value} <- visit_params do
        assert response[key] == expected_value
      end
    end

    test "error: renders errors when data is invalid", %{conn: conn} do
      invalid_params = %{"duration" => nil, "member_id" => nil}
      conn = post(conn, ~p"/api/visits", visit: invalid_params)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "PUT /visits/:id" do
    setup [:create_visit]

    test "success: renders visit when data is valid", %{conn: conn, visit: %Visit{id: id} = visit} do
      expected_tasks = ["sweet-task-1", "cool-task-2"]

      update_params = %{
        "tasks" => expected_tasks,
        "duration" => 43,
        "visit_requested_date" => "2029-03-12"
      }

      conn = put(conn, ~p"/api/visits/#{visit}", visit: update_params)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, ~p"/api/visits/#{id}")

      assert %{
               "id" => ^id,
               "duration" => 43,
               "tasks" => ^expected_tasks,
               "visit_requested_date" => "2029-03-12"
             } = json_response(conn, 200)["data"]
    end

    test "error: renders errors when data is invalid", %{conn: conn, visit: visit} do
      invalid_attrs = %{
        "duration" => nil,
        "tasks" => nil,
        "visit_requested_date" => nil,
        "member_id" => nil
      }

      conn = put(conn, ~p"/api/visits/#{visit}", visit: invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "DELETE /visits/:id" do
    setup [:create_visit]

    test "success: deletes chosen visit", %{conn: conn, visit: visit} do
      conn = delete(conn, ~p"/api/visits/#{visit}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/visits/#{visit}")
      end
    end
  end

  defp create_visit(_) do
    user = Factory.insert(:user_with_minutes)
    visit = Factory.insert(:visit, member_id: user.id)
    %{visit: visit}
  end
end
