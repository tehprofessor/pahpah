defmodule PahPahWeb.VisitFulfillmentControllerTest do
  use PahPahWeb.ConnCase

  alias PahPah.HomeVisits.VisitFulfillment

  setup %{conn: conn} do
    member = Factory.insert(:user_with_minutes)
    pal = Factory.insert(:user)
    visit = Factory.insert(:visit, member_id: member.id)

    {:ok,
     conn: put_req_header(conn, "accept", "application/json"),
     member: member,
     pal: pal,
     visit: visit}
  end

  describe "GET /visit_fulfillments" do
    test "success: returns an empty list when there are no visit fulfillments", %{conn: conn} do
      conn = get(conn, ~p"/api/visit_fulfillments")
      assert json_response(conn, 200)["data"] == []
    end

    test "success: returns a list of all the visits fulfillments", %{conn: conn} do
      fulfillments =
        Enum.map(0..3, fn _ -> create_visit_fulfillment([]).visit_fulfillment end)
        |> Enum.sort_by(& &1.updated_at, :desc)

      conn = get(conn, ~p"/api/visit_fulfillments")

      assert results = json_response(conn, 200)["data"]

      for {result, fulfillment} <- Enum.zip(results, fulfillments),
          {key, actual_value} <- result do
        expected_value =
          case Map.get(fulfillment, String.to_atom(key)) do
            %Date{} = date_value -> Date.to_iso8601(date_value)
            val -> val
          end

        assert actual_value == expected_value
      end
    end
  end

  describe "POST /visit_fulfillments" do
    test "success: renders visit_fulfillment when data is valid", %{
      conn: conn,
      pal: pal,
      member: member,
      visit: visit
    } do
      params =
        Factory.string_params_for(:visit_fulfillment,
          pal_id: pal.id,
          member_id: member.id,
          visit_id: visit.id
        )

      conn = post(conn, ~p"/api/visit_fulfillments", visit_fulfillment: params)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, ~p"/api/visit_fulfillments/#{id}")
      assert response = json_response(conn, 200)["data"]

      for {key, expected_value} <- params do
        assert response[key] == expected_value
      end
    end

    test "error: renders errors when data is missing required values", %{conn: conn} do
      invalid_params = %{member_id: nil, visit_id: nil, pal_id: nil}
      conn = post(conn, ~p"/api/visit_fulfillments", visit_fulfillment: invalid_params)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "PUT /visit_fulfillment/:id" do
    setup [:create_visit_fulfillment]

    test "success: updates the visit_fulfillment when data is valid", %{
      conn: conn,
      visit_fulfillment: %VisitFulfillment{id: id} = visit_fulfillment
    } do
      expected_completed_at =
        DateTime.utc_now() |> DateTime.add(1_001, :second) |> DateTime.to_iso8601()

      conn =
        put(conn, ~p"/api/visit_fulfillments/#{visit_fulfillment}",
          visit_fulfillment: %{completed_at: expected_completed_at}
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, ~p"/api/visit_fulfillments/#{id}")
      assert response = json_response(conn, 200)["data"]

      assert response["completed_at"] == expected_completed_at
    end

    test "error: renders errors when data is invalid", %{
      conn: conn,
      visit_fulfillment: visit_fulfillment
    } do
      invalid_params = %{member_id: nil, visit_id: nil, pal_id: nil}

      conn =
        put(conn, ~p"/api/visit_fulfillments/#{visit_fulfillment}",
          visit_fulfillment: invalid_params
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "DELETE /visit_fulfillment/:id" do
    setup [:create_visit_fulfillment]

    test "success: deletes chosen visit_fulfillment", %{
      conn: conn,
      visit_fulfillment: visit_fulfillment
    } do
      conn = delete(conn, ~p"/api/visit_fulfillments/#{visit_fulfillment}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/visit_fulfillments/#{visit_fulfillment}")
      end
    end
  end

  def create_visit_fulfillment(_) do
    member = Factory.insert(:user_with_minutes)
    pal = Factory.insert(:user)
    visit = Factory.insert(:visit, member_id: member.id)

    %{
      visit_fulfillment:
        Factory.insert(:visit_fulfillment,
          member_id: member.id,
          visit_id: visit.id,
          pal_id: pal.id
        )
    }
  end
end
