# PahPah

An example home visit application.

## Overview

<!-- TOC -->
* [PahPah](#pahpah)
  * [Overview](#overview)
  * [Setup](#setup)
    * [Pre-Requisites](#pre-requisites)
    * [Running Locally](#running-locally)
    * [Configuration](#configuration)
  * [JSON Endpoints](#json-endpoints)
    * [Users](#users)
    * [Visits](#visits)
    * [Visit Fulfillments](#visit-fulfillments)
  * [CLI](#cli)
  * [Notes](#notes)
<!-- TOC -->

## Setup

### Pre-Requisites

* `postgresql` is installed
* `elixir` - `1.14.3` with OTP version `>= 25`
* `erlang` - `25.3` recommended

### Running Locally

* Run `mix setup` will install and setup dependencies, create and migrate the database, and build any assets.
  * Start `PahPah` with `mix phx.server` or inside IEx with `iex -S mix phx.server`
* Verify setup by running the test suite with `mix test`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

### Configuration

The following environment variables may be set to configure your _local_ environment (the values shown are the current defaults):

* `PAH_PAH_DEV_DB_USERNAME=postgres` - The Postgres username
* `PAH_PAH_DEV_DB_PASSWORD=postgres` - The Postgres user's password
* `PAH_PAH_DEV_DB_HOSTNAME=localhost` - The hostname where Postgres is running
* `PAH_PAH_DEV_DB_DATABASE=pah_pah_dev` - The name of the `PahPah` application's database
* `PORT=4000` - The port to use when running the local `PahPah` server
* `PAH_PAH_DEFAULT_MINUTES_AVAILABLE=0` - The default amount of minutes available when creating a user
* `PAH_PAH_FEE_PERCENT=15.0` - The fee *percentage*

## JSON Endpoints

The application is intended to be used over `JSON`, and can be easily accessed using `curl`

```shell
# Example using curl to create a user via the API
curl -XPOST http://localhost:4003/api/users \
  -H 'Content-Type: application/json' \
  --data '{"user": {"id": "e9998672-fc2c-4ce6-bc60-dd4ba4a8d83f", "first_name": "Laszlo", "last_name": "Cravensworth", "email": "laszlo@cravensworth.example.com", "minutes_available": 100}}'
```

### Users

* `GET` - `/api/users` - List all users
* `GET` - `/api/users/:id` - Show a specific user by id
* `PUT` - `/api/users/:id` - Update a user by id
* `POST` - `/api/users` - Create a new user
* `DELETE` - `/api/users` - Delete a user

**Request Data**
```json5

{
  "user":
  {
    "id": "e9998672-fc2c-4ce6-bc60-dd4ba4a8d83f",
    "first_name": "Laszlo",
    "last_name": "Cravensworth",
    "email": "laszlo@cravensworth.example.com",
    "minutes_available": 100,
  }
}
```

### Visits

Represent a request for home service

* `GET` - `/api/visits` - List all visits
* `GET` - `/api/visits/:id` - Show a specific visit by id
* `PUT` - `/api/visits/:id` - Update a visit by id
* `POST` - `/api/visits` - Create a new visit
* `DELETE` - `/api/visits` - Delete a visit

**Response Data**
```json5
{
  "visit":
  {
    "id": "5527372a-5e4e-4dc8-b533-e2b85a1f92d2",
    // user id of the person requested the visit
    "member_id": "c68f764a-d1e4-4d62-b498-0aa5fe474ba6",
    // duration in minutes
    "duration": 100,
    // the tasks for the visit
    "tasks": ["do something", "and something else"],
    // date of requested visit in ISO 8601 format
    "visit_requested_date": "2023-10-31",
  }
}
```

### Visit Fulfillments

Represent a transaction, or a _pal_ going to fulfill a Visit for a member.

* `GET` - `/api/visit_fulfillments` - List all visit_fulfillments
* `GET` - `/api/visit_fulfillments/:id` - Show a specific visit by id
* `PUT` - `/api/visit_fulfillments/:id` - Update a visit by id
* `POST` - `/api/visit_fulfillments` - Create a new visit
* `DELETE` - `/api/visit_fulfillments` - Delete a visit

**Request Data**
```json5
{
  "visit_fulfillment":
  {
    "id": "1dfc150f-c555-4110-bd91-d2cb89ef3306",
    // user id of the person who requested the visit
    "member_id": "c68f764a-d1e4-4d62-b498-0aa5fe474ba6",
    // user id of the person who fulfilled the visit
    "pal_id": "a49dcfea-2ce2-4627-8480-2b0da2dbcdfc",
    // duration in minutes
    "task_id": "e6806be9-8a38-421b-9ac3-434ab7207ebf",
    // the datetime the visit was fulfilled
    "completed_at": "2023-03-14T03:28:13.227864Z",
  }
}
```
## CLI

There are several mix tasks available, as an alternative to the JSON API.

```shell
> mix help | grep pah_pah
mix pah_pah.users.create   # Create a new user
mix pah_pah.users.get      # Create a new user
mix pah_pah.visits.create  # Create a new Visit
mix pah_pah.visits.fulfill # Create a new VisitFulfillment
```

Each task has a moduledoc which can be viewed in IEx by using the `h` helper function:

```elixir
iex> h Mix.Tasks.PahPah.Visits.Create

Create a new Visit from the command line.

## Usage

Basic example creating a visit for a member by email:

    mix pah_pah.visits.create example@example.com \
      --duration 100 \
      --visit-requested-date 2030-01-01 \
      --tasks="do something" \
      --tasks="heck yeah"

Or by using their user id:

    mix pah_pah.visits.create example@example.com \
      --member-id=608ab941-955b-4782-ab41-6f9c25febc7f \
      --duration 100 \
      --visit-requested-date 2030-01-01 \
      --tasks="do something" \
      --tasks="heck yeah"
```

## Notes

**Re: Auth**

Authentication and authorization were omitted to focus on the core
functionality. Typically, over JSON, I'd auth via a JWT using the
`Authorization-Bearer` header and read it from a Plug (middleware) setting
a `private` value in the `conn`.

**Re: Nicer UX**

A welcome email on registration would have been a nice touch, but I opted
for the mix tasks. FWIW, I would have likely just used an async task with
a supervisor, e.g.

```elixir
send_email = fn -> EmailClient.send_it!(user.email) end
# Assuming the `PahPah.WelcomeEmailSupervisor` has been started
Task.Supervisor.async(PahPah.WelcomeEmailSupervisor, send_email)
```

Users can also end up with a negative balance of minutes, this is by design to provide a better experience. Going
forward I would instrument and report on it so users can be helped and the design choice can be monitored.

**Re: Foreign key constraints**

I didn't use any of Ecto's relationship macros like `belongs_to` or `has_many`
mostly out of habit. Lately, I tend to prefer to explicitly perform the `join`
myself when I want to (more or less) preload values. There's absolutely a time
and case for them, but data integrity needs of *this* application are pretty
darn low.

**Re: Routes**

I would've setup *more* restful routes, and properly nested some the resources
to help isolate and restrict access.

E.g. Scoping the `visits` index to a `user_id` from the path.

`/api/users/:user_id/visits`

I'm also not a fan of versioning APIs via a _path_ segment, I prefer to do so
via a header sent by the client.

**Re: Errs**

Due to the nature of this, I've leaned heavily on ecto's changeset errors for
rendering different error cases. There are probably a few instances where doing
something more explicit would provide a better developer experience for the API.
